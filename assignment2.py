from flask import Flask, render_template, request, redirect, url_for, jsonify, json
import random

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
	return json.dumps(books)

@app.route('/')
@app.route('/book/')
def showBook():
	return render_template("showBook.html", books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
	if request.method == "POST":
		if len(books) == 0:
			books.append({"title": request.form['newBookName'], 'id': 1})
			return redirect(url_for("showBook"))
		newID = int(books[-1].get("id")) + 1
		books.append({"title": request.form['newBookName'], 'id': newID})
		return redirect(url_for("showBook"))
	else:
		return render_template("newBook.html")

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
	if request.method == "POST":
		for book in books:
			if int(book['id']) == book_id:
				book["title"] = request.form['editedName']
		return redirect(url_for("showBook"))
	else:
		return render_template("editBook.html", book_id = book_id)
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
	if request.method == "POST":
		for book in books:
			if int(book['id']) == book_id:
				books.remove(book)
		return redirect(url_for("showBook"))
	else:
		return render_template("deleteBook.html", book_id = book_id)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	

